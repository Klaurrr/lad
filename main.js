let str = `Старший братец ПОНЕДЕЛЬНИК –
работяга, не бездельник.
Он неделю открывает
всех трудиться зазывает.

ВТОРНИК следует за братом
у него идей богато.

А потом СРЕДА-сестрица,
не пристало ей лениться.

Брат ЧЕТВЕРГ и так, и сяк,
он мечтательный чудак.

ПЯТНИЦА-сестра сумела
побыстрей закончить дело.

Предпоследний брат СУББОТА
не выходит на работу.

В гости ходит ВОСКРЕСЕНЬЕ,
очень любит угощенье
`;



let body = document.getElementById('page')

const newStr = str => {
    return (
        str.replace('ПОНЕДЕЛЬНИК', 'MONDAY')
        .replace('ВТОРНИК','TUESDAY')
        .replace('СРЕДА', 'WEDNESDAY')
        .replace('ЧЕТВЕРГ', 'THURSDAY')
        .replace('ПЯТНИЦА', 'FRIDAY')
        .replace('СУББОТА', 'SATURDAY')
        .replace('ВОСКРЕСЕНЬЕ', 'SUNDAY')
    )
}



// -----------------------------------------------------------------------------




const compareNumbers = (num1, num2) => {

    let match = 0;
    let noMatch = 0;

    num1 = String(num1).split('')
    num2 = String(num2).split('')

    for(let i = 0; i < num1.length; i++) {

        if(num1[i] === num2[i]) { match++ } 

        else if (num2.indexOf(num1[i]) >= 0) { noMatch++ }

    }

    return `Количество совпавших цифр не на своих местах - ${noMatch}, а количество совпавших цифр на своих местах - ${match}`

}



const letsPlay = (compNum, userNum) => {
    return compNum === userNum ? 'Вы угадали! Поздравляю.' : compareNumbers(compNum, userNum)

}





const monster = {
    maxHealth: 10,
    name: "Лютый",
    moves: [
        {
            "name": "Удар когтистой лапой",
            "physicalDmg": 3, 
            "magicDmg": 0,    
            "physicArmorPercents": 20, 
            "magicArmorPercents": 20,  
            "cooldown": 0     
        },
        {
            "name": "Огненное дыхание",
            "physicalDmg": 0,
            "magicDmg": 4,
            "physicArmorPercents": 0,
            "magicArmorPercents": 0,
            "cooldown": 3
        },
        {
            "name": "Удар хвостом",
            "physicalDmg": 2,
            "magicDmg": 0,
            "physicArmorPercents": 50,
            "magicArmorPercents": 0,
            "cooldown": 2
        },
    ]
}


const player = {
    maxHealth: 10,
    name: "Евстафий",
    moves: [
        {
            "name": "Удар боевым кадилом",
            "physicalDmg": 2,
            "magicDmg": 0,
            "physicArmorPercents": 0,
            "magicArmorPercents": 50,
            "cooldown": 0
        },
        {
            "name": "Вертушка левой пяткой",
            "physicalDmg": 4,
            "magicDmg": 0,
            "physicArmorPercents": 0,
            "magicArmorPercents": 0,
            "cooldown": 4
        },
        {
            "name": "Каноничный фаербол",
            "physicalDmg": 0,
            "magicDmg": 5,
            "physicArmorPercents": 0,
            "magicArmorPercents": 0,
            "cooldown": 3
        },
        {
            "name": "Магический блок",
            "physicalDmg": 0,
            "magicDmg": 0,
            "physicArmorPercents": 100,
            "magicArmorPercents": 100,
            "cooldown": 4
        },
    ]
}

function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function play() {
    let number;
    let health;
    let monsterCooldown1 = 0;
    let monsterCooldown2 = 0;
    let playerCooldown1 = 0;
    let playerCooldown2 = 0;
    let playerCooldown3 = 0;

    number = prompt('Начальное здоровье равно 10. Хотите изменить?\n1. Да\n2. Нет')


    if (number == 1) {
        health = prompt('Введите начальное здоровье')

        monster.maxHealth = health;
        player.maxHealth = health;
    }

    while (monster.maxHealth > 0 && player.maxHealth > 0) {
        let randomNumber;
        let numberPlayer;

        do {
            randomNumber = getRandom(0, 2);

        } while ((randomNumber === 1 && monsterCooldown1 > 0 && monsterCooldown1 < 3) || (randomNumber === 2 && monsterCooldown2 > 0 && monsterCooldown2 < 2));

        if (monsterCooldown1 > 0 && monsterCooldown1 < 3) {
            monsterCooldown1++;
        } else if (monsterCooldown1 === 3) {
            monsterCooldown1 = 0;
        }

        if (monsterCooldown2 > 0 && monsterCooldown2 < 2) {
            monsterCooldown2++;
        } else if (monsterCooldown2 === 2) {
            monsterCooldown2 = 0;
        }

        switch (randomNumber) {
            case 1:
                if (monsterCooldown1 === 0) {
                    monsterCooldown1++;
                }
                break;
            case 2:
                if (monsterCooldown2 === 0) {
                    monsterCooldown2++;
                }
                break;
            default:
                break;
        }

        alert(`\n\nДействие Лютого: ${monster.moves[randomNumber].name}`);

        numberPlayer = prompt('\nВыберите действие:\n1. Удар боевым кадилом;\n2. Вертушка левой пяткой;\n3. Каноничный фаербол;\n4. Магический блок.');


        while ((numberPlayer === 2 && playerCooldown1 > 0 && playerCooldown1 < 4) || (numberPlayer === 3 && playerCooldown2 > 0 && playerCooldown2 < 3) || (numberPlayer === 4 && playerCooldown3 > 0 && playerCooldown3 < 4)) {
            numberPlayer = prompt('Вы пока еще не можете выбрать это действие.');
        }

        if (playerCooldown1 > 0 && playerCooldown1 < 4) {
            playerCooldown1++;
        } else if (playerCooldown1 === 4) {
            playerCooldown1 = 0;
        }

        if (playerCooldown2 > 0 && playerCooldown2 < 3) {
            playerCooldown2++;
        } else if (playerCooldown2 === 3) {
            playerCooldown2 = 0;
        }

        if (playerCooldown3 > 0 && playerCooldown3 < 4) {
            playerCooldown3++;
        } else if (playerCooldown3 === 4) {
            playerCooldown3 = 0;
        }

        switch (numberPlayer - 1) {
            case 1:
                if (playerCooldown1 === 0) {
                    playerCooldown1++;
                }
                break;
            case 2:
                if (playerCooldown2 === 0) {
                    playerCooldown2++;
                }
                break;
            case 3:
                if (playerCooldown3 === 0) {
                    playerCooldown3++;
                }
                break;
            default:
                break;
        }

        player.maxHealth = player.maxHealth - (monster.moves[randomNumber].physicalDmg - (player.moves[numberPlayer - 1].physicArmorPercents / 100 * monster.moves[randomNumber].physicalDmg)) - (monster.moves[randomNumber].magicDmg - (player.moves[numberPlayer - 1].magicArmorPercents / 100 * monster.moves[randomNumber].magicDmg));
        monster.maxHealth = monster.maxHealth - (player.moves[numberPlayer - 1].physicalDmg - (monster.moves[randomNumber].physicArmorPercents / 100 * player.moves[numberPlayer - 1].physicalDmg)) - (player.moves[numberPlayer - 1].magicDmg - (monster.moves[randomNumber].magicArmorPercents / 100 * player.moves[numberPlayer - 1].magicDmg));

        if (player.maxHealth > 0) {
            alert(`Ваше здоровье: ${player.maxHealth.toFixed(1)}`);
        }
        if (monster.maxHealth > 0) {
            alert(`Здоровье Лютого: ${monster.maxHealth.toFixed(1)}`);
        }
    }

    if (monster.maxHealth < player.maxHealth) {
        alert('Вы выиграли. Поздравляю!');
    } else {
        alert('Вы проиграли.');
    }
}

play()


body.innerHTML = 
    `<h1>Задание 1</h1>
        <p>${newStr(str)}</p>
     <h1>Задание 2 - в песочнице</h1>
     <h1>Задание 3</h1>
        <p>
            ${letsPlay(512378, 582437)}
        </p>`